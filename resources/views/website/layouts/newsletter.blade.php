    <!-- Newsletter section start -->
    <div id="rs-newsletter" class="rs-newsletter style2 home11-style pt-25 sm-pt-10 pb-25 sm-pb-15">
        <div class="container">
            <div class="newsletter-wrap">
                <div class="row y-middle">
                    <div class="col-lg-8 col-md-12 md-mb-25 sm-mb-15">
                        <div class="sec-title3">
                            <h2 class="title white-color md-mb-15 sm-mb-5">{{trans('home.newsletter.title')}}</h2>
                            <div class="sub-title white-color">{{trans('home.newsletter.sub-title')}}</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 text-right">
                        <form class="newsletter-form d-inline-block">
                            <input name="email" placeholder="{{trans('home.newsletter.email_placeholder')}}" required onClick="this.setSelectionRange(0, this.value.length);">
                            <button type="button" data-toggle="tooltip" data-placement="top" title="{{trans('home.newsletter.submit')}}">{{trans('home.newsletter.submit')}}</button>
                            <p class="form_error" id="invalid_email">Địa chỉ email không được bỏ trống!</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Newsletter section end -->