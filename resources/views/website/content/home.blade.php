@extends('website.layouts.default')
@section('css-custom')
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="/home/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="/home/css/responsive.css">
@endsection
@section('content')
    @include('website.content.home_list')
@endsection
@section('modal-custom')
<!-- Custom Modal Start -->
<!-- Custom Modal End -->
@endsection
@section('js-custom')

@endsection