@extends('website.layouts.default')
@section('css-custom')
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="/home/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="/home/css/responsive.css">
    <style>
        input[required="required"]:valid + .placeholder,
        input[required="required"]:focus + .placeholder {
            display: none;
        }
        input#fname:hover + .placeholder span::after {
            content: "(*) = không được bỏ trống";
        }
        .placeholder {
            position: absolute;
            top: 12px;
            left: 46px;
            color: #6c757d;
            font-size: 16px;
            line-height: inherit;
        }
        .placeholder span::after {
            color: red;
            content: "(*)";
        }
    </style>
@endsection
@section('content')
    @include('website.layouts.breadcrumbs')
    @include('website.layouts.register')
    @include('website.layouts.newsletter')
@endsection
@section('modal-custom')
<!-- Custom Modal Start -->
<!-- Custom Modal End -->
@endsection
@section('js-custom')

@endsection